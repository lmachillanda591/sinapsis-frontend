var gulp = require('gulp'),
  jsmin = require('gulp-jsmin'),
  rename = require('gulp-rename'),
  minifyCSS = require('gulp-minify-css'),
  concatCss = require('gulp-concat-css'),
  htmlmin = require('gulp-htmlmin'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat');

gulp.task('js', function () {
  gulp.src('app/editor/editor.js')
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('app/editor/min'));
  gulp.src('app/admin/admin.js')
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('app/admin/min'));
  gulp.src('app/home/home.js')
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('app/home/min'));
  gulp.src('app/photo/photo.js')
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('app/photo/min'));
  gulp.src('app/contact/contact.js')
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('app/contact/min'));
  gulp.src(['app/admin/min/admin.min.js', 'app/editor/min/editor.min.js', 'app/home/min/home.min.js', 'app/photo/min/photo.min.js', 'app/contact/min/contact.min.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('app'));
});

gulp.task('css', function () {
  gulp.src('app/app.css')
    .pipe(concatCss("app.min.css"))
    .pipe(minifyCSS({keepBreaks:false}))
    .pipe(gulp.dest('app/'));
});

gulp.task('html', function() {
  gulp.src('app/editor/editor.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/editor/min'));
  gulp.src('app/home/home.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/home/min'));
  gulp.src('app/admin/admin.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/admin/min'));
  gulp.src('app/photo/photo.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/photo/min'));
  gulp.src('app/contact/contact.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/contact/min'));
});
'use strict';

angular.module('myApp.routes', ['ngRoute', 'ui.router'])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/auth');
  $stateProvider
  .state('app', {
    url: '/app',
    templateUrl: 'assets/dev/views/doctors/dashboard.html'
  })
  .state('auth', {
    url: '/auth',
    templateUrl: 'assets/dev/views/system/auth.html',
    controller: 'SignInCtrl',
    params: { requireAuth: false }
  })
  .state('dashboard', {
    url: '/dashboard',
    templateUrl: 'assets/dev/views/doctors/dashboard.html',
    controller: 'DashboardCtrl'
  })
}])
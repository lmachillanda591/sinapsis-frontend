'use strict';

angular.module('myApp.dashboardFactory', [])
.factory('dashboardFactory', function($http, API_LINK){
  return {
    get: function(id) {
    	return $http({
      	method: 'GET',
      	url: 'http://dev.sinapsis.com.ve/api/v1/dashboards/' + id + '.json',
      	headers: {
        'Content-type': 'application/json'
        //'authToken': authToken
    		}
    	})
    } 
    //$dashboard, $consultation, $importance, $request, $doctor, $doctor_consulting
  };
});

'use strict';angular.module('myApp.admin',['ngRoute']).controller('adminCtrl',['$scope','$http','hotelFactory','orderByFilter',function($scope,$http,hotelFactory,orderBy){}]);

'use strict';angular.module('myApp.editor',['ngRoute']).controller('editorCtrl',['$scope','$http','$rootScope','hotelFactory','$filter','orderByFilter','$sce','$timeout',function($scope,$http,$rootScope,hotelFactory,$filter,orderBy,$sce,$timeout){$scope.fullScreen=true;window.onbeforeunload=function(){return"Did you save your stuff?"}
let editorHtml=ace.edit("editorHtml")
editorHtml.setTheme("ace/theme/vibrant_ink");let HtmlScriptMode=ace.require("ace/mode/html").Mode;editorHtml.session.setMode(new HtmlScriptMode());editorHtml.getSession().on('change',function(){$timeout(function(){$scope.getValue();},10);});let editorCss=ace.edit("editorCss")
editorCss.setTheme("ace/theme/merbivore");let CssScriptMode=ace.require("ace/mode/css").Mode;editorCss.session.setMode(new CssScriptMode());editorCss.getSession().on('change',function(){$timeout(function(){$scope.getValue();},10);});let editorJs=ace.edit("editorJs")
editorJs.setTheme("ace/theme/merbivore");let JsScriptMode=ace.require("ace/mode/javascript").Mode;editorJs.session.setMode(new JsScriptMode());editorJs.getSession().on('change',function(){$timeout(function(){$scope.getValue();},10);});$scope.getValue=value=>{let myFirstCodeHtml=editorHtml.getSession().getValue();let myCodeCss=editorCss.getSession().getValue();let myCodeJs=editorJs.getSession().getValue();let finalEditor=ace.edit("finalEditor");let finalScriptMode=ace.require("ace/mode/html").Mode;finalEditor.setValue(myFirstCodeHtml+'<style>'+myCodeCss+'</style>'+'<script>'+myCodeJs+'</script>');finalEditor.session.setMode(new finalScriptMode());finalEditor.$blockScrolling=Infinity;let myCodeHtml=finalEditor.getSession().getValue();let iframeExist=document.getElementById('thisHtml').getElementsByTagName('iframe')
if(iframeExist[0]){createIframe("Exist");}
else{createIframe();}
function createIframe(param){var target=document.getElementById('thisHtml');if(param==="Exist"){target.getElementsByTagName('iframe')[0].src='data:text/html;charset=utf-8,'+encodeURI(myCodeHtml);}
else{let iframe=document.createElement('iframe');iframe.style.width="100%";iframe.style.border="none";iframe.style.height="400px";iframe.style.overflow="scroll";iframe.src='data:text/html;charset=utf-8,'+encodeURI(myCodeHtml);target.appendChild(iframe);}}}
$scope.fullScreenIframe=function(param){let target=document.getElementById('thisHtml').getElementsByTagName('iframe');if(target[0]){param=='up'?(target[0].style.height='800px'):(target[0].style.height='400px');}}}])

'use strict';angular.module('myApp.home',['ngRoute']).controller('homeCtrl',['$scope','$http','hotelFactory','orderByFilter',function($scope,$http,hotelFactory,orderBy){}]);


'use strict';angular.module('myApp.contact',['ngRoute']).controller('contactCtrl',['$scope','$http','hotelFactory','orderByFilter',function($scope,$http,hotelFactory,orderBy){}]);
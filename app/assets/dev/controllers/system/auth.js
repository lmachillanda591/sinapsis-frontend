angular.module("myApp.signInCtrl", [])
.controller('SignInCtrl', ['$rootScope', '$scope', '$state', 'tokenService', 'authFactory',
 	function ($rootScope, $scope, $state, tokenService, authFactory) {
 		/* Jquery */
 			/* Agregar tooltips a elementos truncados */
				$(function(){
					function tooltipsTruncated(e){
						var anyElement = $(e.target);
						var pElement;
						var truncateElement;
						var cloneElement;
						var parentElement;
						var indexElment;
						var whoShow;

					if (anyElement.is('p') || anyElement.is('a')) {
						pElement = anyElement.attr('data-pointer');
						whoShow = anyElement.attr('data-json');
					}
					if (pElement != undefined) {
						truncateElement = anyElement;
						cloneElement = truncateElement.clone().css({display: 'inline', width: 'auto', 'position':'absolute', 'bottom':'0px', 'visibility':'hidden'}).appendTo('body');

						if (pElement == 'text-trunc-label-h') {
							var boxWidth = ((($(window).width()*0.495)*0.35)-50)-50
							if (cloneElement.width() > boxWidth) {
								truncateElement.attr({'data-toggle':'tooltip-header-1', 'title':'Estas ubicado en:'})
							}
						}else if(pElement == 'actual-consulting-room-title'){
							var boxWidth = ((($(window).width()*0.495)*0.35)-50)-50
							if (cloneElement.width() > boxWidth) {	
								var titleJson = $scope.actual_consulting_room.title;
								truncateElement.attr({'data-toggle':'tooltip-header-2', 'title':titleJson})	
							}
						}else if(pElement.includes('consulting-room-')){
							indexElment = pElement.substring(16,17);
							var boxWidth = (((($(window).width()*0.495)*0.35)-50)-50)-2
							if (cloneElement.width() > boxWidth) {	
								var titleJson = $scope.consulting_rooms[indexElment].title;
								var consultingNameJson = $scope.consulting_rooms[indexElment].consulting_name;
								truncateElement.attr({'data-toggle':'tooltip-dashb', 'title':titleJson + ' ' + consultingNameJson})	
							}
						}else if (pElement.includes('text-trunc-consult-')) {
							indexElment = pElement.substring(19,20) - 1;
							parentElement = truncateElement.parent('div');
							if (cloneElement.width() > parentElement.width()) {
								if (whoShow == 'room-title') {
									var titleJson = $rootScope.rooms[indexElment].title
									truncateElement.attr({'data-toggle':'tooltip-dashb', 'title':titleJson})
								}else{
									var nameJson = $rootScope.rooms[indexElment].name
									truncateElement.attr({'data-toggle':'tooltip-dashb', 'title':nameJson})
								}
							}
						}else if (pElement.includes('text-trunc-profile')) {
							parentElement = truncateElement.parent('div');
							if (cloneElement.width() > parentElement.width()) {
								var nameJson = $rootScope.currentUser.full_name
								truncateElement.attr({'data-toggle':'tooltip-doct-name', 'title':nameJson})
							}
						}else if(pElement.includes('text-trunc-doct-')){
							indexElment = pElement.substring(16,17);
							parentElement = truncateElement.parent('div');
							if (cloneElement.width() > parentElement.width()) {	
								var nameJson = $scope.allDoctors[indexElment].doctor.full_name;
								truncateElement.attr({'data-toggle':'tooltip-dashb', 'title':nameJson})	
							}
						}else if(pElement.includes('profile-box')){
							parentElement = truncateElement.parent('div');
							if (cloneElement.width() > parentElement.width()) {	
								if(whoShow == 'doctor-name-1'){
									var nameJson = $rootScope.currentUser.full_name
									truncateElement.attr({'data-toggle':'tooltip-profile-0', 'title':nameJson})
								}else if (whoShow == 'doctor-name-2') {
									var nameJson = $rootScope.currentUser.full_name
									truncateElement.attr({'data-toggle':'tooltip-profile-1', 'title':nameJson})
								}else{
									var emailJson = $rootScope.currentUser.email
									truncateElement.attr({'data-toggle':'tooltip-profile-2', 'title':emailJson})
								}
							}
						}		
					}
				}
					$(window).mouseover(function (e) {
						tooltipsTruncated(e);
					})
					$(window).resize(function(){
						$(window).mouseover(function (e) {
							tooltipsTruncated(e);
						})
					})
				})
			/* Agregar tooltips a elementos truncados */	


	 		$('#app').css('height', '100vh');

	 		/* Despliegue de la caja info de usuario */
				$('.user-popover').popover(animation='false');
			 	var open=false;
			 	$('.box-h-s-5').click(function(){
			 		if (open==false) {
			 			$('.user-box').css('display', 'block');
			 			open = true;
			 		}else{
			 			$('.user-box').css('display', 'none');
			 			open = false;
			 		}
			 	});	
		 	/* End Despliegue */

		 	var open3 = false;

		 	/* Calculamos el width actual constantemente */
			 	var widthWindow = $(window).innerWidth();
			 	var everythingHide = $('aside').css('display');
			 	$( window ).resize(function() {
				  widthWindow = $(window).width();
				  everythingHide = $('aside').css('display');
				  $('aside').removeAttr('style');
				  if (open3 == true) {
				  	$('#back-icon').css('display', 'block');
				  	$('#burger-bar').css('display', 'none');
				  }else{
				  	$('#back-icon').css('display', 'none');
				  	$('#burger-bar').css('display', 'block');
				  }
				});
			/* End Calculo del width */

			/* Abrir y ocultar indicadores textaules de los navegadores */
				var navBoxOpen = false;
			 	var open2 = false;
			 	$('.burger-icon').click(function(){
			 		if (widthWindow < 991 ) {
			 			if (open3 == false) {
			 				$('aside').css('display', 'block');
			 				$('#i-o-navs').css({'transition':'1s', 'left':'55px'});
				      navBoxOpen = true;
			 				open3	= true;
			 				$('#burger-bar').css('display', 'none');
			 				$('#back-icon').css('display', 'block');
			 			}else{
			 				$('aside').css('display', 'none');
			 				$('#i-o-navs').css({'transition':'1s', 'left':'-200px'});
				      navBoxOpen = false;
			 				open3	= false;
			 				$('#burger-bar').css('display', 'block');
			 				$('#back-icon').css('display', 'none');
			 			}
			 		}else{
			 			if (navBoxOpen == false) {
				      $('#i-o-navs').css({'transition':'1s', 'left':'55px'});
				      navBoxOpen = true;
				      $('#burger-bar').css('display', 'none');
			 				$('#back-icon').css('display', 'block');
				    }else{
				      $('#i-o-navs').css({'transition':'1s', 'left':'-200px'});
				      navBoxOpen = false;
				      $('#burger-bar').css('display', 'block');
			 			$('#back-icon').css('display', 'none');
				    }
			 		}
			 	});
		 	/* Abri y ocultar indicadores textaules de los navegadores */
		/* Jquery */
 		$scope.user = {};
 		$scope.login = function() {
 			var user = {"user":{"email": $scope.user.email, "password": $scope.user.password}}
 			authFactory.signIn(user).then(function (user) {
 				$rootScope.currentUser = user;
 				tokenService.updateMetaTag();
 				user.patient ? ( // se hizo condicional anidado ya que existen solo dos condiciones, otras condiciones se aplicaria lo comentado abajo
 					$state.go('app'),
 					$rootScope.goTo = 'app',
 					$rootScope.isPatient = true,
 					$rootScope.isDoctor = false
 					) :
 					(
					authFactory.getAll().then(function(response){
						console.log(response.data);
					}),
 					//$state.go('app.patients.dashboard'), //this is 
 					$rootScope.isDoctor = true,
 					$rootScope.isPatient = false
 					)
 				//--- else if (user.patient == null && user.doctor == null) {} else {$rootScope.isDoctorAndPatient = true;}
 			}, function (error) {
 				//toaster.pop('error', 'Error', error.data.error);
 			console.log("pablo escobar");
 			})
 		}
 		$scope.logout = function () {
 			Auth.logout().then(function (oldUser) {
 				$rootScope.currentUser = undefined;
 				$state.go('access.signin');
 				tokenService.updateMetaTag();
 			}, function (error) {
 				toaster.pop('error', 'Error', 'Ocurrió un error al salir del sistema');
 			})
 		}
 	}
])
angular.module('myApp.tokenService', [])
.factory('tokenService', ['$http', function ($http) {
	return {
		updateMetaTag: function () {
			$http.get('/authenticity_token').then(function (response) {
				$('meta[name=csrf-token]').attr('content', response.data)
			}, function (error) {
				console.log(error)
				//toaster.pop('error', 'Error', 'Ocurrió un error al obtener el Token')
			})
		}
	}
}])
'use strict'
angular.module("myApp.DashboardCtrl", [])
.controller	('DashboardCtrl', ['$scope', '$rootScope', '$state', '$dashboard', '$consultation', '$importance', '$request', '$doctor', '$doctor_consulting',
		function ($scope, $rootScope, $state, $dashboard, $consultation, $importance, $request, $doctor, $doctor_consulting) {
	
	$scope.getAllDoctors = function(){
		$doctor.index().then(function(response){
			$scope.allDoctors = response.data
			$scope.allDoctors.forEach(function(doctor, index){
				if (doctor.doctor.doctor_id == $rootScope.currentUser.doctor.id) {
					$scope.allDoctors.splice(index, 1)
				}
			})
			shuffle($scope.allDoctors)
		})
	} 

	function shuffle(array) {
	  var currentIndex = array.length, temporaryValue, randomIndex;
	  // While there remain elements to shuffle...
	  while (0 !== currentIndex) {
	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;
	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	  }
	  return array;
	}
	$scope.getAllDoctors();

	$scope.getNumber = function(num) {
	    return new Array(num);   
	}

	$scope.noRooms = true

	$scope.getDashboard = function () {
		if ($rootScope.currentUser.doctor) {
			$dashboard.get($rootScope.currentUser.doctor.id).then(function (response) {
				$scope.dashboard = response.data
				$rootScope.patient_list = $scope.dashboard.patient_list
				$rootScope.dailyPatients = $scope.dashboard.daily_patients
				$rootScope.rooms = $scope.dashboard.consulting_rooms
				if ($scope.rooms) {
					$scope.noRooms = false
				} 
				$scope.todayPatients($rootScope.dailyPatients, $rootScope.patient_list)
			})
		}
	}

	$scope.todayPatients = function (array1,array2) {
		$rootScope.finalList = []
		if (array1[0] && array2[0]) {
			for (var i = 0; i < array1.length; i++) {
				for (var j = 0; j < array2.length; j++) {
					if (array1[i].patient_id == array2[j].patient_id) {
						var patient = {
							id: array2[j].patient_id,
							cropped_image: array2[j].cropped_image,
							first_name: array2[j].first_name,
							last_name: array2[j].last_name,
							email: array2[j].email
						}
						$rootScope.finalList.push(patient)
					}
				}
			}
		}
	}

	(function(){
			$scope.date = new Date()
			$scope.day = $scope.date.getDate();
			var indexArrayFind = $scope.date.getMonth();
			var arrayMonth = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
			$scope.month = arrayMonth[indexArrayFind-1]
			$scope.dayAndMonth = $scope.day + ' ' + $scope.month; 
		}
	)();
	$scope.getDashboard()
}])

'use strict';
// Declare app level module which depends on views, and components
angular.module('myApp', ['ngRoute', 'myApp.version', 'myApp.routes', 'ui.router', 'myApp.constants', 'myApp.authFactory', 'myApp.signInCtrl', 'myApp.tokenService', 'myApp.DashboardCtrl'])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  //$locationProvider.hashPrefix('!');
  //$routeProvider.otherwise({redirectTo: '/editor'});
}]);